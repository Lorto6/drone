﻿using System;
using System.Threading;
using Raspberry.IO.Components.Controllers.Pca9685;
using RPi.Pwm;
using RPi.Pwm.Motors;
using UnitsNet;

namespace Drone
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Frequency updateRate = Frequency.FromHertz(50);
                Pca9685DeviceFactory df = new Pca9685DeviceFactory();
                df.PwmFrequency = updateRate;
                IPwmDevice controller = df.GetDevice();
                controller.SetPwmUpdateRate(updateRate);

                int servoMin = 205;
                int servoMax = 409;
                ServoMotor sm1 = new ServoMotor(controller, PwmChannel.C0, servoMin, servoMax);
                ServoMotor sm2 = new ServoMotor(controller, PwmChannel.C1, servoMin, servoMax);
                ServoMotor sm3 = new ServoMotor(controller, PwmChannel.C2, servoMin, servoMax);
                ServoMotor sm4 = new ServoMotor(controller, PwmChannel.C3, servoMin, servoMax);

                sm1.MoveTo(0);
                sm2.MoveTo(0);
                sm3.MoveTo(0);
                sm4.MoveTo(0);
            }
            catch (Exception e)
            {
                Console.WriteLine("Main: " + e.Message);
            }
        }
    }
}